<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\CronJob;

use Exception;
use GuzzleHttp\Exception\GuzzleException;
use JTL\Cron\Job;
use JTL\Cron\JobInterface;
use JTL\Cron\QueueEntry;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\HttpClient\PPCClient;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequestException;
use Plugin\jtl_paypal_commerce\PPC\Tracking\Tracker;
use Plugin\jtl_paypal_commerce\PPC\Tracking\TrackersRequest;
use Plugin\jtl_paypal_commerce\PPC\Tracking\TrackersResponse;

/**
 * Class CronJob
 * @package Plugin\jtl_paypal_commerce\CronJob
 */
class CronJob extends Job
{
    /**
     * @inheritDoc
     */
    public function start(QueueEntry $queueEntry): JobInterface
    {
        parent::start($queueEntry);
        $this->garbageCollect();
        $this->setFinished($this->sendTrackingInfo());

        return $this;
    }

    /**
     * @return void
     */
    private function garbageCollect(): void
    {
        $this->db->executeQuery(
            'DELETE FROM xplugin_jtl_paypal_checkout_shipment_state
                WHERE delivery_date < CURDATE()'
        );
    }

    /**
     * @return bool
     */
    private function sendTrackingInfo(): bool
    {
        $rowCounter = $this->db->getSingleObject(
            'SELECT COUNT(*) AS cnt
                FROM xplugin_jtl_paypal_checkout_shipment_state
                WHERE status_sent = 0'
        );
        $states     = $this->db->getCollection(
            'SELECT id, transaction_id, tracking_id, carrier, shipment_date
                FROM xplugin_jtl_paypal_checkout_shipment_state
                WHERE status_sent = 0
                ORDER BY delivery_date
                LIMIT 10'
        );

        if ($rowCounter === null || $states->count() === 0) {
            return true;
        }

        $trackers = [];
        $client   = new PPCClient(PPCHelper::getEnvironment());
        foreach ($states as $state) {
            $trackers[] = (new Tracker())
                ->setTransactionId($state->transaction_id)
                ->setTrackingNumber($state->tracking_id)
                ->setShipmentDateAsString($state->shipment_date)
                ->setCarrier($state->carrier)
                ->setStatus(Tracker::STATE_SHIPPED);
        }
        try {
            $trackingResponse = new TrackersResponse(
                $client->send(new TrackersRequest(Token::getInstance()->getToken(), $trackers))
            );
            foreach ($trackingResponse->getErrors() as $error) {
                if ($error->getName() !== 'RESOURCE_NOT_FOUND' && $error->getName() !== 'INPUT_VALIDATION_ERROR') {
                    $this->logger->error('TrackersRequest failed (' . $error->getMessage() . ')');
                    continue;
                }
                foreach ($error->getDetails() as $detail) {
                    if (!\str_contains($detail->getField(), '/transaction_id')) {
                        continue;
                    }
                    $this->db->delete(
                        'xplugin_jtl_paypal_checkout_shipment_state',
                        'transaction_id',
                        $detail->getValue()
                    );
                }
            }
        } catch (PPCRequestException $e) {
            $this->logger->error('TrackersRequest failed (' . $e->getResponse()->getMessage() . ')');
        } catch (Exception | GuzzleException $e) {
            $this->logger->error('TrackersRequest failed (' . $e->getMessage() . ')');
        }

        $this->db->executeQuery(
            'UPDATE xplugin_jtl_paypal_checkout_shipment_state
                SET status_sent = 1
                WHERE id IN (' . $states->implode('id', ', ') . ')'
        );

        return $rowCounter->cnt <= 10;
    }
}
