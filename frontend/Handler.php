<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use DateTime;
use Exception;
use JTL\Alert\Alert;
use JTL\Checkout\Bestellung;
use JTL\Consent\Item;
use JTL\DB\DbInterface;
use JTL\DB\ReturnType;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Helpers\Request;
use JTL\IO\IO;
use JTL\IO\IOError;
use JTL\IO\IOResponse;
use JTL\Link\LinkInterface;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\LegacyHelper;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\paymentmethod\PaymentStateResult;
use Plugin\jtl_paypal_commerce\paymentmethod\PPCP\OrderNotFoundException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\MerchantCredentials;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\paymentmethod\PaymentmethodNotFoundException;
use Plugin\jtl_paypal_commerce\PPC\Logger;
use Plugin\jtl_paypal_commerce\PPC\Order\AppContext;
use Plugin\jtl_paypal_commerce\paymentmethod\PayPalPaymentInterface;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Order\ShippingChangeResponse;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;

/**
 * Class Handler
 * @package Plugin\jtl_paypal_commerce\frontend
 */
final class Handler
{
    /** @var PluginInterface */
    private PluginInterface $plugin;

    /** @var DbInterface */
    private DbInterface $db;

    /** @var Configuration */
    private Configuration $config;

    /**
     * Handler constructor.
     * @param PluginInterface    $plugin
     * @param DbInterface|null   $db
     * @param Configuration|null $configuration
     */
    public function __construct(PluginInterface $plugin, ?DbInterface $db = null, ?Configuration $configuration = null)
    {
        $this->plugin = $plugin;
        $this->db     = $db ?? Shop::Container()->getDB();
        $this->config = $configuration ?? PPCHelper::getConfiguration($plugin);
    }

    /**
     * @param string $original
     * @return string
     */
    public static function getBackendTranslation(string $original): string
    {
        if (!Shop::isFrontend()) {
            return \__($original);
        }

        $getText   = Shop::Container()->getGetText();
        $oldLocale = $getText->getLanguage();
        $locale    = Helper::sanitizeLocale(Helper::getLocaleFromISO(Helper::sanitizeISOCode(Shop::Lang()->getIso())));
        if ($oldLocale !== $locale) {
            $getText->setLanguage($locale);
            $translate = \__($original);
            $getText->setLanguage($oldLocale);
        } else {
            $translate = \__($original);
        }

        return $translate;
    }

    /**
     * @return void
     */
    public function pageStepShipping(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::STEP_SHIPPING);
    }

    /**
     * @return void
     */
    public function pageStepPayment(): void
    {
        CheckoutPage::getInstance($this->plugin)->validatePayment(Shop::Smarty());
    }

    /**
     * @return void
     */
    public function pageStepConfirm(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::STEP_CONFIRM);
    }

    /**
     * @return void
     */
    public function pageStepAddress(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::STEP_ADDRESS);
    }

    /**
     * @return void
     */
    public function pageStepProductDetails(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::PRODUCT_DETAILS);
    }

    /**
     * @return void
     */
    public function pageStepCart(): void
    {
        CheckoutPage::getInstance($this->plugin)->setPageStep(CheckoutPage::CART);
    }

    /**
     * @return void
     */
    public function pageCustomerAccount(): void
    {
        $orderId = Request::getInt('bestellung');
        if ($orderId > 0) {
            $order = new Bestellung($orderId);
            if ($order->kBestellung === null || $order->kBestellung === 0) {
                return;
            }
            $helper    = Helper::getInstance($this->plugin);
            $payMethod = $helper->getPaymentFromID($order->kZahlungsart);
            if ($payMethod === null) {
                return;
            }

            $payMethod->getFrontendInterface($this->config, Shop::Smarty())->renderOrderDetailPage($order, $payMethod);
        }
    }

    /**
     * @param PayPalPaymentInterface $payMethod
     * @param bool                   $timeout
     * @return PaymentStateResult|null
     */
    private function getPaymentStateResult(PayPalPaymentInterface $payMethod, bool $timeout): ?PaymentStateResult
    {
        $result = new PaymentStateResult(null, null, null, $timeout);

        try {
            $payMethod->onPendingCapture();
            $ppOrder = $payMethod->getPPOrder();
            if ($ppOrder === null) {
                throw new OrderNotFoundException();
            }
        } catch (OrderNotFoundException) {
            return null;
        }

        switch ($payMethod->getValidOrderState($ppOrder)) {
            case OrderStatus::STATUS_PENDING:
                // Order will be approved... waiting and check again
                $result->setPendingMessage(self::getBackendTranslation('Ihre Zahlung wird überprüft'));
                $result->setCompleteMessage(
                    $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_pending_info')
                );

                break;
            case OrderStatus::STATUS_APPROVED:
                // Order is approved and will be captured... waiting and check again
                $result->setPendingMessage(self::getBackendTranslation(
                    'Ihre Zahlung wurde genehmigt und die Bestellung wird jetzt erfasst'
                ));

                break;
            case OrderStatus::STATUS_COMPLETED:
                // Order is captured and will be finalized... waiting and check again
                $result->setPendingMessage(self::getBackendTranslation(
                    'Ihre Zahlung wurde erfasst und die Bestellung wird jetzt abgeschlossen'
                ));

                break;
            case OrderStatus::STATUS_DECLINED:
                $result->setCompleteMessage(
                    \sprintf(
                        $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_declined'),
                        $ppOrder->getInvoiceId(),
                        $payMethod->getLocalizedPaymentName()
                    )
                );

                break;
        }
        $result->setRedirect($ppOrder->getLink('paymentRedirect'));
        $result->setState($payMethod->getValidOrderState($ppOrder));
        $payMethod->onPaymentState($result, $timeout);

        return $result;
    }

    /**
     * @param LinkInterface $link
     * @param JTLSmarty     $smarty
     * @return void
     */
    public function checkPaymentState(LinkInterface $link, JTLSmarty $smarty): void
    {
        $helper = Helper::getInstance($this->plugin);
        $cUID   = Request::getVar('uid');
        if ($cUID !== null) {
            $state     = $this->db->queryPrepared(
                'SELECT tbestellstatus.kBestellung, tbestellung.kZahlungsart
                    FROM tbestellstatus
                    INNER JOIN tbestellung ON tbestellstatus.kBestellung = tbestellung.kBestellung
                    WHERE tbestellstatus.cUID = :cuid',
                ['cuid' => $cUID],
                ReturnType::SINGLE_OBJECT
            );
            $payMethod = $state ? $helper->getPaymentFromID((int)$state->kZahlungsart) : null;
        } else {
            $payMethod = $helper->getPaymentFromID(Request::getInt('payment'))
                ?? $helper->getPaymentFromName('PayPalCommerce');
        }
        if ($payMethod === null) {
            Helper::redirectAndExit(Shop::Container()->getLinkService()->getStaticRoute('jtl.php') . '?bestellungen=1');
            exit();
        }

        $paymentState = $this->getPaymentStateResult($payMethod, Request::hasGPCData('timeout'));
        $ppOrder      = $payMethod->getPPOrder();
        $shopOrder    = $ppOrder !== null ? $helper->getShopOrder($ppOrder) : null;
        if ($paymentState === null) {
            Helper::redirectAndExit($payMethod->getReturnURL($shopOrder ?? new Bestellung()));
        }
        if ($paymentState->hasRedirect() && $paymentState->getRedirect() !== $payMethod->getPaymentStateURL()) {
            if ($paymentState->hasCompleteMessage()) {
                Shop::Container()->getAlertService()->addInfo(
                    $paymentState->getCompleteMessage(),
                    'paymentState',
                    ['saveInSession' => true]
                );
            }
            Helper::redirectAndExit($paymentState->getRedirect());
        }

        $link->setTitle($payMethod->getLocalizedPaymentName());
        $smarty->assign('waitingBackdrop', !$paymentState->isTimeout())
               ->assign('checkMessage', $paymentState->hasPendingMessage()
                   ? $paymentState->getPendingMessage()
                   : self::getBackendTranslation('Ihre Zahlung wird überprüft'))
               ->assign('methodID', $payMethod->getMethod()->getMethodID())
               ->assign('orderStateURL', $payMethod->getReturnURL($shopOrder ?? new Bestellung()));
    }

    /**
     * @param int  $methodID
     * @param bool $timeout
     * @return object
     */
    public function checkIOPaymentState(int $methodID, bool $timeout): object
    {
        $result    = new IOResponse();
        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromID($methodID);
        if ($payMethod === null) {
            $result->setClientRedirect(
                Shop::Container()->getLinkService()->getStaticRoute('jtl.php') . '?bestellungen=1'
            );

            return $result;
        }

        $helper       = Helper::getInstance($this->plugin);
        $paymentState = $this->getPaymentStateResult($payMethod, $timeout);
        $ppOrder      = $payMethod->getPPOrder();
        $shopOrder    = $ppOrder !== null ? $helper->getShopOrder($ppOrder) : null;
        if ($paymentState === null) {
            $result->setClientRedirect($payMethod->getReturnURL($shopOrder ?? new Bestellung()));

            return $result;
        }
        if ($paymentState->hasRedirect() && $paymentState->getRedirect() !== $payMethod->getPaymentStateURL()) {
            if ($paymentState->hasCompleteMessage()) {
                Shop::Container()->getAlertService()->addInfo(
                    $paymentState->getCompleteMessage(),
                    'paymentState',
                    ['saveInSession' => true]
                );
            }
            $result->setClientRedirect($paymentState->getRedirect());
        } else {
            $result->assignDom('pp-loading-body span', 'innerHTML', $paymentState->hasPendingMessage()
                ? $paymentState->getPendingMessage()
                : self::getBackendTranslation('Ihre Zahlung wird überprüft'));
        }

        return $result;
    }

    /**
     * @param string      $fundingSource
     * @param string|null $payment
     * @param string|null $bnCode
     * @param array|null  $formData
     * @return object
     */
    public function createIOPPOrder(
        string $fundingSource,
        ?string $payment = null,
        ?string $bnCode = null,
        ?array $formData = null
    ): object {
        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromName($payment ?? 'PayPalCommerce');
        if ($payMethod === null) {
            return new IOError('Paymentmethod not found');
        }

        $payMethod->unsetCache();
        $payMethod->setFundingSource($fundingSource);
        $result = new IOResponse();

        if (($formData !== null)
            && ((int)($formData['versandartwahl'] ?? 0) === 1 || (int)($formData['Versandart'] ?? 0) > 0)
            && !LegacyHelper::pruefeVersandartWahl((int)$formData['Versandart'], $formData)
        ) {
            $alertSrvc = Shop::Container()->getAlertService();
            $alert     = $alertSrvc->getAlert('fillShipping');
            if ($alert !== null) {
                $alertSrvc->removeAlertByKey('fillShipping');
                $result->assignVar('createResult', $alert->getMessage());
            } else {
                $result->assignVar('createResult', Shop::Lang()->get('fillShipping', 'checkout'));
            }

            return $result;
        }
        $ppcOrderId = $payMethod->createPPOrder(
            Frontend::getCustomer(),
            Frontend::getCart(),
            $fundingSource,
            AppContext::SHIPPING_FROM_FILE,
            AppContext::PAY_CONTINUE,
            $bnCode ?? MerchantCredentials::BNCODE_EXPRESS
        );
        if ($ppcOrderId === null) {
            $logger = new Logger(Logger::TYPE_PAYMENT, $payMethod);
            $logger->write(\LOGLEVEL_NOTICE, 'createIOPPOrder - PayPal order can not be created.');

            $alertService = Shop::Container()->getAlertService();
            $alert        = $alertService->getAlert('createOrderRequest');
            if ($alert !== null) {
                $result->assignVar('createResultDetails', $alert->getMessage());
                unset($_SESSION['alerts']['createOrderRequest']);
            }
            $result->assignVar(
                'createResult',
                self::getBackendTranslation('Die Zahlung konnte nicht bei PayPal angefragt werden')
            );
        } else {
            $result->assignVar('orderId', $ppcOrderId);
        }

        return $result;
    }

    /**
     * @param array       $data
     * @param string|null $payment
     * @return object
     */
    public function shippingIOChange(array $data, ?string $payment = null): object
    {
        $payMethod = Helper::getInstance($this->plugin)->getPaymentFromName($payment ?? 'PayPalCommerce');
        if ($payMethod === null) {
            return new IOError('Paymentmethod not found');
        }

        $result        = new IOResponse();
        $ppOrder       = $payMethod->getPPOrder();
        $patchShipping = false;
        try {
            $objData  = \json_encode($data, \JSON_FORCE_OBJECT | \JSON_THROW_ON_ERROR);
            $shipping = new ShippingChangeResponse(\json_decode($objData, false, 128, \JSON_THROW_ON_ERROR));
        } catch (\JsonException $e) {
            $logger = new Logger(Logger::TYPE_INFORMATION);
            $logger->write(
                \LOGLEVEL_ERROR,
                $this->plugin->getPluginID() . '::shippingIOChange - JSON-Error (' . $e->getMessage() . ')'
            );
            $shipping = null;
        }
        if ($ppOrder !== null && $shipping !== null && $ppOrder->getId() === $shipping->getOrderID()) {
            $address = $shipping->getShippingAddress();
            if ($address !== null) {
                $patch = $payMethod->handleShippingData($ppOrder, $shipping);
                if ($patch !== null) {
                    $patchShipping = true;
                    $result->assignVar('op', $patch->getOp())
                           ->assignVar('path', $patch->getPath())
                           ->assignVar('value', $patch->getValue());
                }
            }
        }

        return $result->assignVar('patch', $patchShipping);
    }

    /**
     * @return void
     */
    public function handleECSOrder(): void
    {
        $linkHelper = Shop::Container()->getLinkService();
        $payMethod  = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        try {
            if ($payMethod === null) {
                throw new PaymentmethodNotFoundException('Paymentmethod PayPalCommerce not found');
            }
            $expressCheckout = new ExpressCheckout($payMethod, $this->config);
            if (!$expressCheckout->ecsCheckout()) {
                Shop::Container()->getAlertService()->addAlert(
                    Alert::TYPE_NOTE,
                    $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_ecs_missing_data'),
                    'missingPayerData',
                    [
                        'saveInSession' => true,
                        'linkText'      => $payMethod->getLocalizedPaymentName(),
                    ]
                );
                Helper::redirectAndExit(
                    $linkHelper->getStaticRoute('bestellvorgang.php')
                    . ((int)Frontend::getCustomer()->kKunde === 0 ? '?unreg_form=1' : '')
                );
                exit();
            }

            Helper::redirectAndExit($linkHelper->getStaticRoute('bestellvorgang.php'));
            exit();
        } catch (Exception $e) {
            Shop::Container()->getAlertService()->addAlert(
                Alert::TYPE_ERROR,
                self::getBackendTranslation($e->getMessage()),
                'ppcNotFound',
                ['saveInSession' => true]
            );

            Helper::redirectAndExit($linkHelper->getStaticRoute('warenkorb.php'));
            exit();
        }
    }

    /**
     * @param array $args
     * @return void
     * @throws CircularReferenceException | ServiceNotFoundException | PaymentmethodNotFoundException
     */
    public function smarty(array $args): void
    {
        $checkoutPage = CheckoutPage::getInstance($this->plugin);
        /** @var JTLSmarty $smarty */
        $smarty = $args['smarty'];

        if ($checkoutPage->getPageStep() !== CheckoutPage::STEP_UNKNOWN) {
            $checkoutPage->render($smarty);
        } else {
            $payment  = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
            $frontend = new PayPalFrontend($this->plugin, PPCHelper::getConfiguration($this->plugin), $smarty);
            if ($payment === null) {
                throw new PaymentmethodNotFoundException('Paymentmethod PayPalCommerce not found');
            }
            if ($payment->isValid(Frontend::getCustomer(), Frontend::getCart())) {
                $frontend->preloadInstalmentBannerJS(CheckoutPage::PAGE_SCOPE_MINICART);
                $frontend->preloadECSJS(CheckoutPage::PAGE_SCOPE_MINICART);
                $components = $frontend->renderMiniCartComponents(
                    $payment,
                    [],
                    Frontend::getCustomer(),
                    Frontend::getCart()
                );
                $frontend->renderPayPalJsSDK($components, $payment->getBNCode(), false, true);
            }
        }
    }

    /**
     * @param array $args
     */
    public function ioRequest(array $args): void
    {
        /** @var IO $io */
        $io = $args['io'];
        try {
            $io->register('jtl_paypal_commerce.checkPaymentState', [$this, 'checkIOPaymentState']);
            $io->register('jtl_paypal_commerce.createOrder', [$this, 'createIOPPOrder']);
            $io->register('jtl_paypal_commerce.shippingChange', [$this, 'shippingIOChange']);
        } catch (Exception $e) {
            $logger = new Logger(Logger::TYPE_INFORMATION);
            $logger->write(
                \LOGLEVEL_ERROR,
                $this->plugin->getPluginID() . '::ioRequest - can not register io handler (' . $e->getMessage() . ')'
            );
        }
    }

    /**
     * @param array $args
     */
    public function addConsentItem(array $args): void
    {
        $lastID   = $args['items']->reduce(static function ($result, Item $item) {
                $value = $item->getID();
                return $result === null || $value > $result ? $value : $result;
        }) ?? 0;
        $locale   = $this->plugin->getLocalization();
        $cmActive = Shop::getSettingValue(\CONF_CONSENTMANAGER, 'consent_manager_active') ?? 'N';
        if ($cmActive === 'Y' &&
            $this->config->getPrefixedConfigItem(
                BackendUIsettings::BACKEND_SETTINGS_SECTION_CONSENTMANAGER . '_activate'
            ) === 'Y'
        ) {
            $langISO = Shop::getLanguageCode();
            $item    = new Item();
            $item->setName(self::getBackendTranslation('PayPal Express Checkout und Ratenzahlung'));
            $item->setID(++$lastID);
            $item->setItemID(Configuration::CONSENT_ID);
            $item->setDescription($locale->getTranslation(
                'jtl_paypal_commerce_instalment_banner_consent_description',
                $langISO
            ));
            $item->setPurpose($locale->getTranslation(
                'jtl_paypal_commerce_instalment_banner_consent_purpose',
                $langISO
            ));
            $item->setPrivacyPolicy(
                'https://www.paypal.com/de/webapps/mpp/ua/privacy-full?locale.x=' .
                Helper::sanitizeLocale(Helper::getLocaleFromISO(Helper::sanitizeISOCode(Shop::Lang()->getIso())))
            );
            $item->setCompany('PayPal');
            $args['items']->push($item);
        }
    }

    /**
     * @param array $args
     * @return void
     */
    public function saveOrder(array $args): void
    {
        /** @var Bestellung $order */
        $order     = $args['oBestellung'];
        $helper    = Helper::getInstance($this->plugin);
        $payMethod = $helper->getPaymentFromID((int)$order->kZahlungsart);
        if ($payMethod !== null) {
            $payMethod->finalizeOrderInDB($order);
        }
    }

    /**
     * @param array $args
     * @return void
     * @throws Exception
     */
    public function updateOrder(array $args): void
    {
        /** @var Bestellung $order */
        $order = $args['oBestellung'];
        $wawi  = $args['oBestellungWawi'];
        $sent  = empty($wawi->dVersandt) ? null : new DateTime($wawi->dVersandt);
        $now   = new DateTime();

        if (empty($wawi->cIdentCode)
            || $sent === null
            || $now->diff($sent)->format('%a') > \BESTELLUNG_VERSANDBESTAETIGUNG_MAX_TAGE
        ) {
            return;
        }
        $paymentIds = [];
        foreach ($this->plugin->getPaymentMethods()->getMethods() as $method) {
            $paymentIds[] = $method->getMethodID();
        }
        $payments = $this->db->getCollection(
            'SELECT tbestellung.kBestellung, COALESCE(cMap.carrier_paypal, tbestellung.cLogistiker) AS carrier,
                    tzahlungseingang.cHinweis AS transaction_id,
                    tbestellung.dVersandDatum AS shipmentDate,
                    ADDDATE(tbestellung.dVersandDatum, INTERVAL tversandart.nMaxLiefertage DAY) AS deliveryDate
                FROM tbestellung
                INNER JOIN tzahlungseingang
                    ON tzahlungseingang.kBestellung = tbestellung.kBestellung
                LEFT JOIN xplugin_jtl_paypal_checkout_carrier_mapping AS cMap
                    ON cMap.carrier_wawi = tbestellung.cLogistiker
                INNER JOIN tversandart
                    ON tversandart.kVersandart = tbestellung.kVersandart
                WHERE tbestellung.kBestellung = :orderId
                    AND tbestellung.kZahlungsart IN (' . \implode(', ', $paymentIds) . ")
                    AND tzahlungseingang.cHinweis RLIKE '^[A-Z0-9]+$'",
            [
                'orderId' => $order->kBestellung,
            ]
        );
        foreach ($payments as $payment) {
            $this->db->upsert('xplugin_jtl_paypal_checkout_shipment_state', (object)[
                'transaction_id' => $payment->transaction_id,
                'tracking_id'    => $wawi->cIdentCode,
                'carrier'        => $payment->carrier,
                'shipment_date'  => $payment->shipmentDate,
                'delivery_date'  => $payment->deliveryDate,
                'status_sent'    => 0,
            ], ['transaction_id']);
        }
    }
}
