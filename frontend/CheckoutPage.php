<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\frontend;

use Exception;
use JTL\Customer\CustomerGroup;
use JTL\Exceptions\CircularReferenceException;
use JTL\Exceptions\ServiceNotFoundException;
use JTL\Helpers\ShippingMethod;
use JTL\Plugin\PluginInterface;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;

/**
 * Class CheckoutPage
 * @package Plugin\jtl_paypal_commerce\frontend
 */
final class CheckoutPage
{
    /** @var PluginInterface */
    private PluginInterface $plugin;

    /** @var Configuration */
    private Configuration $config;

    /** @var int|null */
    private ?int $pageStep = null;

    /** @var self[] */
    public static array $instance = [];

    public const STEP_UNKNOWN    = 0;
    public const STEP_SHIPPING   = 1;
    public const STEP_CONFIRM    = 2;
    public const PRODUCT_DETAILS = 3;
    public const CART            = 4;
    public const STEP_ADDRESS    = 5;

    public const PAGE_SCOPE_PRODUCTDETAILS = 'productDetails';
    public const PAGE_SCOPE_MINICART       = 'miniCart';
    public const PAGE_SCOPE_CART           = 'cart';
    public const PAGE_SCOPE_ORDERPROCESS   = 'orderProcess';
    public const PAGE_SCOPES               = [
        self::PAGE_SCOPE_PRODUCTDETAILS,
        self::PAGE_SCOPE_MINICART,
        self::PAGE_SCOPE_CART,
        self::PAGE_SCOPE_ORDERPROCESS,
    ];

    /**
     * CheckoutPage constructor.
     * @param PluginInterface    $plugin
     * @param Configuration|null $config
     */
    private function __construct(PluginInterface $plugin, Configuration $config = null)
    {
        $this->plugin = $plugin;
        $this->config = $config ?? PPCHelper::getConfiguration($plugin);

        self::$instance[$plugin->getPluginID()] = $this;
    }

    /**
     * @param PluginInterface $plugin
     * @return self
     */
    public static function getInstance(PluginInterface $plugin): self
    {
        return self::$instance[$plugin->getPluginID()] ?? new self(
            $plugin,
            PPCHelper::getConfiguration($plugin)
        );
    }

    /**
     * @return int
     */
    public function getPageStep(): int
    {
        return $this->pageStep ?? self::STEP_UNKNOWN;
    }

    /**
     * @param int $pageStep
     */
    public function setPageStep(int $pageStep): void
    {
        $this->pageStep = $pageStep;
    }

    /**
     * @param JTLSmarty $smarty
     * @return void
     */
    public function validatePayment(JTLSmarty $smarty): void
    {
        $payment = Helper::getInstance($this->plugin)->getPaymentFromName('PayPalCommerce');
        if ($payment === null) {
            return;
        }

        $ppOrder = $payment->getPPOrder();
        if ($ppOrder === null || $ppOrder->getStatus() !== OrderStatus::STATUS_APPROVED) {
            return;
        }

        // Reset current shipping methods to usable with this payment
        $customer        = Frontend::getCustomer();
        $shippingMethods = $smarty->getTemplateVars('Versandarten');
        foreach (\array_keys($shippingMethods) as $key) {
            if (!$payment->isAssigned(
                ShippingMethod::getShippingClasses(Frontend::getCart()),
                $customer->getGroupID() > 0
                    ? $customer->getGroupID()
                    : CustomerGroup::getDefaultGroupID(),
                $shippingMethods[$key]->kVersandart
            )) {
                unset($shippingMethods[$key]);
            }
        }
        $smarty->assign('Versandarten', $shippingMethods);

        // Reset current payment methods to only this payment
        $paymentMethods = $smarty->getTemplateVars('Zahlungsarten');
        foreach (\array_keys($paymentMethods) as $key) {
            if ($payment->getMethod()->getMethodID() !== $paymentMethods[$key]->kZahlungsart) {
                unset($paymentMethods[$key]);
            }
        }
        $smarty->assign('Zahlungsarten', $paymentMethods);
    }

    /**
     * @param JTLSmarty $smarty
     * @return void
     * @throws CircularReferenceException
     * @throws ServiceNotFoundException
     */
    public function render(JTLSmarty $smarty): void
    {
        $ppcPayments = [];
        foreach ($this->plugin->getPaymentMethods()->getMethods() as $paymentMethod) {
            $ppcPayment = Helper::getInstance($this->plugin)->getPaymentFromID($paymentMethod->getMethodID());
            if ($ppcPayment !== null) {
                $ppcPayments[] = $ppcPayment;
            }
        }
        $customer = Frontend::getCustomer();
        $cart     = Frontend::getCart();

        try {
            switch ($this->getPageStep()) {
                case self::STEP_SHIPPING:
                    foreach ($ppcPayments as $ppcPayment) {
                        $ppcPayment->getFrontendInterface($this->config, $smarty)
                                   ->renderShippingPage($customer, $cart);
                    }

                    break;
                case self::STEP_ADDRESS:
                    foreach ($ppcPayments as $ppcPayment) {
                        $ppcPayment->getFrontendInterface($this->config, $smarty)
                                   ->renderAddressPage($customer, $cart);
                    }

                    break;
                case self::STEP_CONFIRM:
                    $paymentId = (int)(Frontend::get('Zahlungsart')->kZahlungsart ?? 0);
                    foreach ($ppcPayments as $ppcPayment) {
                        if ($ppcPayment->getMethod()->getMethodID() === $paymentId) {
                            $ppcPayment->getFrontendInterface($this->config, $smarty)
                                       ->renderConfirmationPage($paymentId, $customer, $cart);
                        } else {
                            $ppcPayment->unsetCache();
                        }
                    }

                    break;
                case self::PRODUCT_DETAILS:
                    $product = $smarty->getTemplateVars('Artikel');
                    foreach ($ppcPayments as $ppcPayment) {
                        $ppcPayment->getFrontendInterface($this->config, $smarty)
                                   ->renderProductDetailsPage($customer, $cart, $product);
                    }

                    break;
                case self::CART:
                    foreach ($ppcPayments as $ppcPayment) {
                        $ppcPayment->getFrontendInterface($this->config, $smarty)
                                   ->renderCartPage($customer, $cart);
                    }

                    break;
            }
        } catch (Exception $e) {
            $logger = Shop::Container()->getLogService();
            $logger->addRecord($logger::ERROR, 'page can not be rendered (' . $e->getMessage() . ')');
        }
    }
}
