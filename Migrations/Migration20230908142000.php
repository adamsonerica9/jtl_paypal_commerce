<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\Migrations;

use JTL\Plugin\Migration;
use JTL\Update\IMigration;
use Plugin\jtl_paypal_commerce\PPC\Tracking\Tracker;

/**
 * Class Migration20230908142000
 * @package Plugin\jtl_paypal_commerce\Migrations
 */
class Migration20230908142000 extends Migration implements IMigration
{
    /** @var string */
    protected $description = 'Update carrier mapping';

    /** @var array */
    private array $update = [
        'DE_DEUTSCHE'                              => Tracker::CARRIER_DE_POST,
        'DE_DEUTSHCE_POST_DHL_TRACK_TRACE_EXPRESS' => Tracker::CARRIER_DE_DHL_EXPRESS,
        'DE_DHL_ECOMMERCE'                         => Tracker::CARRIER_DE_DHL_PACKET,
        'DE_DHL_PACKET'                            => Tracker::CARRIER_DE_DHL_PACKET,
        'DE_DPD'                                   => Tracker::CARRIER_DE_DPD,
        'DE_GLS'                                   => Tracker::CARRIER_DE_GLS,
        'DE_HERMES'                                => Tracker::CARRIER_DE_HERMES,
        'DE_TNT'                                   => Tracker::CARRIER_DE_TNT,
        'DE_UPS'                                   => Tracker::CARRIER_DE_UPS,
        'DE_FEDEX'                                 => Tracker::CARRIER_FEDEX,
    ];

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        foreach ($this->update as $old => $new) {
            $this->getDB()->update('xplugin_jtl_paypal_checkout_carrier_mapping', 'carrier_paypal', $old, (object)[
                'carrier_paypal' => $new,
            ]);
        }
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        foreach (\array_flip($this->update) as $old => $new) {
            $this->getDB()->update('xplugin_jtl_paypal_checkout_carrier_mapping', 'carrier_paypal', $old, (object)[
                'carrier_paypal' => $new,
            ]);
        }
    }
}
