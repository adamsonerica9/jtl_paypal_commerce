<?php /** @noinspection PhpMultipleClassDeclarationsInspection */
declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod;

use JTL\Alert\Alert;
use JTL\Cart\Cart;
use JTL\Catalog\Product\Artikel;
use JTL\Checkout\Bestellung;
use JTL\Customer\CustomerGroup;
use JTL\Helpers\GeneralObject;
use JTL\Helpers\Request;
use JTL\Helpers\ShippingMethod;
use JTL\Session\Frontend;
use JTL\Shop;
use JTL\Smarty\JTLSmarty;
use Plugin\jtl_paypal_commerce\frontend\Handler;
use Plugin\jtl_paypal_commerce\frontend\PaymentFrontendInterface;
use Plugin\jtl_paypal_commerce\frontend\PPCPFrontend;
use Plugin\jtl_paypal_commerce\LegacyHelper;
use Plugin\jtl_paypal_commerce\paymentmethod\PPCP\OrderNotFoundException;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCCaptureDecline;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCCapturePending;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCPayerActionRequired;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCServerError;
use Plugin\jtl_paypal_commerce\PPC\Authorization\AuthorizationException;
use Plugin\jtl_paypal_commerce\PPC\Authorization\Token;
use Plugin\jtl_paypal_commerce\PPC\BackendUIsettings;
use Plugin\jtl_paypal_commerce\PPC\Configuration;
use Plugin\jtl_paypal_commerce\PPC\Order\Order;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use Plugin\jtl_paypal_commerce\PPC\Request\PPCRequestException;

/**
 * Class PayPalCommerce
 * @package Plugin\jtl_paypal_commerce\paymentmethod
 */
class PayPalCommerce extends PayPalPayment
{
    /**
     * @inheritDoc
     */
    public function paymentDuringOrderSupported(): bool
    {
        return true;
    }

    /**
     * @inheritDoc
     */
    public function paymentAfterOrderSupported(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    public function mappedLocalizedPaymentName(?string $isoCode = null): string
    {
        return Handler::getBackendTranslation('Standardzahlarten');
    }

    /**
     * @inheritDoc
     */
    protected function usePurchaseItems(): bool
    {
        return false;
    }

    /**
     * @inheritDoc
     */
    protected function isSessionPayed(object $paymentSession): bool
    {
        return (int)$paymentSession->nBezahlt > 0;
    }

    /**
     * @param Order               $ppOrder
     * @param PPCRequestException $exception
     * @return void
     */
    protected function handleOvercharge(Order $ppOrder, PPCRequestException $exception): void
    {
        $detail = $exception->getDetail();
        if ($detail === null || $detail->getIssue() !== OrderStatus::STATUS_PAYER_ACTION_REQUIRED) {
            return;
        }

        $this->resetPPOrder($ppOrder->getId());
        $localization = $this->plugin->getLocalization();
        Shop::Container()->getAlertService()->addAlert(
            Alert::TYPE_ERROR,
            $localization->getTranslation('jtl_paypal_commerce_payer_action_required'),
            'handleOvercharge',
            [
                'saveInSession' => true,
                'linkText'      => $localization->getTranslation('jtl_paypal_commerce_psd2overcharge'),
            ]
        );

        Helper::redirectAndExit(
            Shop::Container()->getLinkService()->getStaticRoute('bestellvorgang.php')
        );
        exit();
    }

    /**
     * @inheritDoc
     */
    public function isValidIntern(array $args_arr = []): bool
    {
        if (!parent::isValidIntern($args_arr)) {
            return false;
        }

        try {
            return $this->method->getDuringOrder() && Token::getInstance()->getToken() !== null;
        } catch (AuthorizationException $e) {
            $this->logger->write(\LOGLEVEL_ERROR, 'AuthorizationException:' . $e->getMessage());

            return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function isValid(object $customer, Cart $cart): bool
    {
        if (Request::postInt('resetPayment') === 1) {
            $this->resetPPOrder();
            unset($_POST['resetPayment']);

            return false;
        }

        return parent::isValid($customer, $cart);
    }

    /**
     * @inheritDoc
     */
    public function isValidExpressPayment(object $customer, Cart $cart): bool
    {
        if (!$this->isValid($customer, $cart)) {
            return false;
        }

        foreach ($cart->PositionenArr as $cartItem) {
            if ((int)($cartItem->Artikel->FunktionsAttribute['no_paypalexpress'] ?? 0) === 1) {
                return false;
            }
        }

        return $this->isAssigned(
            ShippingMethod::getShippingClasses($cart),
            (int)($customer->kKundengruppe ?? 0) > 0
                ? (int)$customer->kKundengruppe
                : CustomerGroup::getDefaultGroupID()
        );
    }

    /**
     * @inheritDoc
     */
    public function isValidExpressProduct(object $customer, ?Artikel $product): bool
    {
        if (!$this->isValidIntern()) {
            return false;
        }

        if ($product === null || $product->bHasKonfig || $product->inWarenkorbLegbar <= 0) {
            return false;
        }

        /** @var Cart $cart */
        $cart       = GeneralObject::deepCopy(Frontend::getCart());
        $dispatcher = $this->helper->revokeHookDispatcher();
        $cart->fuegeEin($product->getID(), \max($product->fMindestbestellmenge, 1), $product->Attribute);
        $this->helper->restoreHookDispatcher($dispatcher);

        return $this->isValidExpressPayment($customer, $cart);
    }

    /**
     * @inheritDoc
     */
    public function isValidBannerPayment(object $customer, Cart $cart): bool
    {
        return $this->isValid($customer, $cart) && $this->isAssigned(
            ShippingMethod::getShippingClasses($cart),
            (int)($customer->kKundengruppe ?? 0) > 0
                ? (int)$customer->kKundengruppe
                : CustomerGroup::getDefaultGroupID()
        );
    }

    /**
     * @inheritDoc
     */
    public function isValidBannerProduct(object $customer, ?Artikel $product): bool
    {
        if (!$this->isValidIntern()) {
            return false;
        }

        if ($product === null || $product->bHasKonfig) {
            return false;
        }

        /** @var Cart $cart */
        $cart = GeneralObject::deepCopy(Frontend::getCart());
        if ($product->inWarenkorbLegbar > 0) {
            $dispatcher = $this->helper->revokeHookDispatcher();
            $cart->fuegeEin($product->getID(), \max($product->fMindestbestellmenge, 1), $product->Attribute);
            $this->helper->restoreHookDispatcher($dispatcher);
        }

        return $this->isValidBannerPayment($customer, $cart);
    }

    /**
     * @inheritDoc
     */
    public function getFrontendInterface(Configuration $config, JTLSmarty $smarty): PaymentFrontendInterface
    {
        return new PPCPFrontend($this->plugin, $this, $smarty);
    }

    /**
     * @inheritDoc
     */
    public function getFundingSource(): string
    {
        return $this->sessionCache->getFundingSource() ?? '';
    }

    /**
     * @inheritDoc
     */
    public function getDefaultFundingSource(): string
    {
        return 'paypal';
    }

    /**
     * @inheritDoc
     */
    public function preparePaymentProcess(Bestellung $order): void
    {
        parent::preparePaymentProcess($order);

        $ppOrder = $this->getPPOrder();
        if ($ppOrder === null || empty($ppOrder->getId())) {
            $this->getLogger()->write(
                \LOGLEVEL_NOTICE,
                'preparePaymentProcess: payment can not be processed, order does not exists'
            );
            $this->raisePaymentError('jtl_paypal_commerce_payment_error', $this->getPaymentRetryURL());
            exit();
        }

        try {
            $ppOrder = $this->verifyPPOrder($ppOrder->getId());
        } catch (PPCRequestException $e) {
            $this->showErrorResponse($e->getResponse(), new Alert(
                Alert::TYPE_ERROR,
                $this->plugin->getLocalization()->getTranslation('jtl_paypal_commerce_payment_error'),
                'preparePaymentProcess',
                ['saveInSession' => true]
            ));
            Helper::redirectAndExit($this->getPaymentRetryURL());
        } catch (OrderNotFoundException) {
            $this->raisePaymentError('jtl_paypal_commerce_payment_error', $this->getPaymentRetryURL());
        }
        $this->getLogger()->write(\LOGLEVEL_DEBUG, 'preparePaymentProcess: verifyOrder', $ppOrder);

        if (!$this->isValidOrderState($ppOrder, OrderStatus::STATUS_APPROVED)) {
            $this->getLogger()->write(\LOGLEVEL_NOTICE, 'preparePaymentProcess: UnexpectedOrderState get '
                . $ppOrder->getStatus() . ' expected ' . OrderStatus::STATUS_APPROVED);
            $this->handleOrder($ppOrder, $order);

            return;
        }

        $orderNumber = LegacyHelper::baueBestellnummer();
        $ppOrder->getPurchase()->setInvoiceId($orderNumber);
        if ($this->config->getPrefixedConfigItem(
            BackendUIsettings::BACKEND_SETTINGS_SECTION_GENERAL . '_purchaseDescription'
        ) === 'N') {
            $ppOrder->getPurchase()->setDescription(
                $this->helper->getSimpleDescription($orderNumber, $this->getShopTitle())
            );
        }
        try {
            (new TCPayerActionRequired())->execute($this, $ppOrder, Frontend::getCustomer(), Frontend::getCart());
            $ppOrder = $this->ppcpOrder->callPatch(new Order($ppOrder->getData()));
        } catch (PPCRequestException | OrderNotFoundException $e) {
            $this->handleOvercharge($ppOrder, $e);
            $this->getLogger()->write(
                \LOGLEVEL_NOTICE,
                'preparePaymentProcess: OrderPatchFailed - ' . $e->getMessage()
            );
            $this->raisePaymentError('jtl_paypal_commerce_payment_error', $this->getPaymentRetryURL());
        }

        try {
            (new TCServerError())->execute($this, $ppOrder, Frontend::getCustomer(), Frontend::getCart());
            $ppOrder = $this->ppcpOrder->callCapture($orderNumber, $this->getBNCode());
            (new TCServerError())->execute($this, $ppOrder, Frontend::getCustomer(), Frontend::getCart());
        } catch (PPCRequestException $e) {
            $this->handleOvercharge($ppOrder, $e);
            $this->getLogger()->write(
                \LOGLEVEL_NOTICE,
                'preparePaymentProcess: OrderCaptureFailed - ' . $e->getMessage()
            );
            // The API-Call failed - try to get the state in case of payment was still succesfully captured
            try {
                (new TCServerError(true))->execute($this, $ppOrder, Frontend::getCustomer(), Frontend::getCart());
                $ppOrder = $this->verifyPPOrder($ppOrder->getId());
                if (!$this->isValidOrderState($ppOrder, OrderStatus::STATUS_COMPLETED)) {
                    Shop::Container()->getAlertService()->addAlert(
                        Alert::TYPE_ERROR,
                        $this->plugin->getLocalization()
                                     ->getTranslation('jtl_paypal_commerce_payment_error'),
                        'orderCaptureFailed',
                        [
                            'saveInSession' => true,
                        ]
                    );
                }
            } catch (PPCRequestException | OrderNotFoundException) {
                // at this point the payment state is realy unknown - persist order to prevent payments without order
                $this->getLogger()->write(
                    \LOGLEVEL_ERROR,
                    'preparePaymentProcess: OrderCaptureFailed twice - persist order',
                    $order
                );
                $this->helper->persistOrder($order, $ppOrder, $this, [
                    'invoiceId' => $orderNumber
                ]);
            }
        } catch (OrderNotFoundException) {
            $this->raisePaymentError('jtl_paypal_commerce_payment_error', $this->getPaymentRetryURL());
        }

        $ppOrder = (new TCCaptureDecline())->execute(
            $this,
            (new TCCapturePending())->execute($this, $ppOrder, Frontend::getCustomer(), Frontend::getCart()),
            Frontend::getCustomer(),
            Frontend::getCart()
        );
        if ($this->isValidOrderState($ppOrder, OrderStatus::STATUS_PENDING)
            || $this->isValidOrderState($ppOrder, OrderStatus::STATUS_COMPLETED)
        ) {
            $this->helper->persistOrder($order, $ppOrder, $this, [
                'invoiceId' => $orderNumber
            ]);
        }

        $this->handleOrder($ppOrder, $order);
    }
}
