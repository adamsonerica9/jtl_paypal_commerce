<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod\PPCP;

use Exception;

/**
 * Class OrderNotFoundException
 * @package Plugin\jtl_paypal_commerce\paymentmethod\PPCP
 */
class OrderNotFoundException extends Exception
{
}
