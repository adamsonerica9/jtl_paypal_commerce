<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\paymentmethod\PPCP;

use RuntimeException;

/**
 * Class InvalidOrderException
 * @package Plugin\jtl_paypal_commerce\paymentmethod\PPCP
 */
class InvalidOrderException extends RuntimeException
{
}
