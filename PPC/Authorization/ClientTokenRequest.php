<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Authorization;

use Plugin\jtl_paypal_commerce\PPC\Request\AuthorizedRequest;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\Nullable;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\SerializerInterface;

/**
 * Class ClientTokenRequest
 * @package Plugin\jtl_paypal_commerce\PPC\Authorization
 */
class ClientTokenRequest extends AuthorizedRequest
{
    /**
     * ClientTokenRequest constructor.
     * @param string $token
     */
    public function __construct(string $token)
    {
        parent::__construct($token);
    }

    /**
     * @inheritDoc
     */
    protected function initBody(): SerializerInterface
    {
        return new Nullable();
    }

    /**
     * @inheritDoc
     */
    protected function getPath(): string
    {
        return '/v1/identity/generate-token';
    }
}
