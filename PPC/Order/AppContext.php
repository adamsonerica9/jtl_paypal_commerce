<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Order;

use InvalidArgumentException;
use Plugin\jtl_paypal_commerce\PPC\PPCHelper;
use Plugin\jtl_paypal_commerce\PPC\Request\Serializer\JSON;

/**
 * Class AppContext
 * @package Plugin\jtl_paypal_commerce\PPC\Order
 */
class AppContext extends JSON
{
    public const SHIPPING_FROM_FILE   = 'GET_FROM_FILE';
    public const SHIPPING_NO_SHIPPING = 'NO_SHIPPING';
    public const SHIPPING_PROVIDED    = 'SET_PROVIDED_ADDRESS';

    public const PAY_CONTINUE = 'CONTINUE';
    public const PAY_NOW      = 'PAY_NOW';

    public const PAGE_LOGIN    = 'LOGIN';
    public const PAGE_BILLING  = 'BILLING';
    public const PAGE_RELEVANT = 'NO_PREFERENCE';

    /**
     * AppContext constructor.
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[
            'user_action'         => self::PAY_NOW,
            'shipping_preference' => self::SHIPPING_PROVIDED,
            'landing_page'        => self::PAGE_RELEVANT,
            'locale'              => 'de-DE',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function setData($data)
    {
        parent::setData($data);

        $payMethod = $this->getData()->payment_method ?? null;
        if ($payMethod !== null && !($payMethod instanceof PayMethod)) {
            $this->setPayMethod((new PayMethod($payMethod)));
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBrandName(): ?string
    {
        return $this->data->brand_name ?? null;
    }

    /**
     * @param string|null $brandName
     * @return AppContext
     */
    public function setBrandName(?string $brandName): self
    {
        if ($brandName === null) {
            unset($this->data->brand_name);
        } else {
            $this->data->brand_name = PPCHelper::shortenStr($brandName, 127);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCancelURL(): ?string
    {
        return $this->data->cancel_url ?? null;
    }

    /**
     * @param string|null $cancelURL
     * @return AppContext
     */
    public function setCancelURL(?string $cancelURL): self
    {
        if ($cancelURL === null) {
            unset($this->data->cancel_url);
        } else {
            $this->data->cancel_url = $cancelURL;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getLandingPage(): string
    {
        return $this->data->landing_page ?? self::PAGE_RELEVANT;
    }

    /**
     * @param string $landingPage
     * @return AppContext
     */
    public function setLandingPage(string $landingPage): self
    {
        if (!\in_array($landingPage, [self::PAGE_LOGIN, self::PAGE_BILLING, self::PAGE_RELEVANT])) {
            throw new InvalidArgumentException(\sprintf('%s is not a valid landing page', $landingPage));
        }

        $this->data->landing_page = $landingPage;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->data->locale;
    }

    /**
     * @param string $locale
     * @return AppContext
     */
    public function setLocale(string $locale): self
    {
        static $pattern = '/^[a-z]{2}(?:-[A-Z][a-z]{3})?(?:-(?:[A-Z]{2}))?$/';

        if (!\preg_match($pattern, $locale)) {
            throw new InvalidArgumentException(\sprintf('%s is not a valid locale', $locale));
        }

        $this->data->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getPayAction(): string
    {
        return $this->data->user_action ?? self::PAY_NOW;
    }

    /**
     * @param string $payAction
     * @return AppContext
     */
    public function setPayAction(string $payAction): self
    {
        if (!\in_array($payAction, [self::PAY_NOW, self::PAY_CONTINUE])) {
            throw new InvalidArgumentException(\sprintf('%s is not a valid user action', $payAction));
        }

        $this->data->user_action = $payAction;

        return $this;
    }

    /**
     * @return PayMethod|null
     */
    public function getPayMethod(): ?PayMethod
    {
        return $this->data->payment_method ?? null;
    }

    /**
     * @param PayMethod|null $payMethod
     * @return AppContext
     */
    public function setPayMethod(?PayMethod $payMethod): self
    {
        if ($payMethod === null) {
            unset($this->data->payment_method);
        } else {
            $this->data->payment_method = $payMethod;
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getReturnURL(): ?string
    {
        return $this->data->return_url ?? null;
    }

    /**
     * @param string|null $returnURL
     * @return AppContext
     */
    public function setReturnURL(?string $returnURL): self
    {
        if ($returnURL === null) {
            unset($this->data->return_url);
        } else {
            $this->data->return_url = $returnURL;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getShipping(): string
    {
        return $this->data->shipping_preference ?? self::SHIPPING_PROVIDED;
    }

    /**
     * @param string $shipping
     * @return AppContext
     */
    public function setShipping(string $shipping): self
    {
        if (!\in_array($shipping, [self::SHIPPING_NO_SHIPPING, self::SHIPPING_FROM_FILE, self::SHIPPING_PROVIDED])) {
            throw new InvalidArgumentException(\sprintf('%s is not a valid shipping preference', $shipping));
        }

        $this->data->shipping_preference = $shipping;

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        $data = clone $this->getData();

        if (empty($data->brand_name)) {
            unset($data->brand_name);
        }

        return $data;
    }
}
