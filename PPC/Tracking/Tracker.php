<?php declare(strict_types=1);

namespace Plugin\jtl_paypal_commerce\PPC\Tracking;

use DateTime;

/**
 * Class Tracker
 * @package Plugin\jtl_paypal_commerce\PPC\Tracking
 */
class Tracker extends TrackerIdentifier
{
    public const STATE_CANCELLED = 'CANCELLED'; // The shipment was cancelled and the tracking number no longer applies
    public const STATE_DELIVERED = 'DELIVERED'; // The item was already delivered when the tracking number was uploaded
    public const STATE_SHIPPED   = 'SHIPPED';   // The item was shipped and is on the way
    public const STATE_ERROR     = 'ERROR';     // An error occurred with the shipment
    public const STATE_UNKNOWN   = '';

    public const STATES = [
        self::STATE_CANCELLED,
        self::STATE_DELIVERED,
        self::STATE_SHIPPED,
        self::STATE_ERROR,
    ];

    public const CARRIER_DE_POST        = 'DEUTSCHE_DE'; // Deutsche Post
    public const CARRIER_DE_DHL_EXPRESS = 'DE_DHL_EXPRESS'; // DHL Express
    public const CARRIER_DE_DHL_PACKET  = 'DHL_API'; // DHL Packet
    public const CARRIER_DE_DPD         = 'DE_DPD_DELISTRACK'; // DPD Germany
    public const CARRIER_DE_GLS         = 'GLS_DE'; // General Logistics Systems (GLS)
    public const CARRIER_DE_HERMES      = 'HERMES_DE'; // Hermes Germany
    public const CARRIER_DE_TNT         = 'TNT_DE'; // TNT Germany
    public const CARRIER_DE_UPS         = 'UPS'; // United Parcel Service
    public const CARRIER_DPE_EXPRESS    = 'DPE_EXPRESS'; // DPE Express
    public const CARRIER_FEDEX          = 'FEDEX'; // Federal Express.
    public const CARRIER_OTHER          = 'OTHER'; // Other.

    public const CARRIERS = [
        self::CARRIER_DE_POST,
        self::CARRIER_DE_DHL_EXPRESS,
        self::CARRIER_DE_DHL_PACKET,
        self::CARRIER_DE_DPD,
        self::CARRIER_DE_GLS,
        self::CARRIER_DE_HERMES,
        self::CARRIER_DE_TNT,
        self::CARRIER_DE_UPS,
        self::CARRIER_DPE_EXPRESS,
        self::CARRIER_FEDEX,
        self::CARRIER_OTHER,
    ];

    /**
     * Tracker constructor
     * @param object|null $data
     */
    public function __construct(?object $data = null)
    {
        parent::__construct($data ?? (object)[]);
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->getData()->status ?? self::STATE_UNKNOWN;
    }

    /**
     * @param string $status
     * @return self
     */
    public function setStatus(string $status): self
    {
        if (\in_array($status, self::STATES)) {
            $this->data->status = $status;
        } else {
            $this->data->status = self::STATE_CANCELLED;
        }

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getShipmentDate(): ?DateTime
    {
        $dateStr = $this->getData()->shipment_date ?? null;
        if ($dateStr === null) {
            return null;
        }

        try {
            return new DateTime($dateStr);
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param DateTime $shipment_date
     * @return self
     */
    public function setShipmentDate(DateTime $shipment_date): self
    {
        $this->data->shipment_date = $shipment_date->format('Y-m-d');

        return $this;
    }

    /**
     * @param string $shipment_date
     * @return self
     */
    public function setShipmentDateAsString(string $shipment_date): self
    {
        $this->data->shipment_date = $shipment_date;

        return $this;
    }

    /**
     * @return string
     */
    public function getCarrier(): string
    {
        $carrier = $this->getData()->carrier ?? self::CARRIER_OTHER;
        if ($carrier === self::CARRIER_OTHER) {
            return $this->getData()->carrier_name_other ?? self::CARRIER_OTHER;
        }

        return $carrier;
    }

    /**
     * @param string $carrier
     * @return self
     */
    public function setCarrier(string $carrier): self
    {
        if (!\in_array($carrier, self::CARRIERS)) {
            $this->data->carrier            = self::CARRIER_OTHER;
            $this->data->carrier_name_other = $carrier;
        } else {
            $this->data->carrier = $carrier;
        }

        return $this;
    }
}
