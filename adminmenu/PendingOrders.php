<?php
/**
 * jtlshop
 * PendingOrders.php
 *
 * @author     Falk Prüfer <falk@fpruefer.de>
 * @package    Plugin\jtl_paypal_commerce\adminmenu
 */

namespace Plugin\jtl_paypal_commerce\adminmenu;

use Illuminate\Support\Collection;
use JTL\DB\DbInterface;
use JTL\Plugin\PluginInterface;
use JTL\Services\JTL\AlertServiceInterface;
use JTL\Shop;
use Plugin\jtl_paypal_commerce\paymentmethod\Helper;
use Plugin\jtl_paypal_commerce\paymentmethod\TestCase\TCCapturePending;
use Plugin\jtl_paypal_commerce\PPC\Order\OrderStatus;
use stdClass;

/**
 * Class PendingOrders
 * @package Plugin\jtl_paypal_commerce\adminmenu
 */
class PendingOrders
{
    /** @var PluginInterface */
    private PluginInterface $plugin;

    /** @var DbInterface */
    private DbInterface $db;

    /**
     * PendingOrders constructor
     * @param PluginInterface $plugin
     * @param DbInterface     $db
     */
    public function __construct(PluginInterface $plugin, DbInterface $db)
    {
        $this->plugin = $plugin;
        $this->db     = $db;
    }

    /**
     * @return Collection
     */
    public function getPendingOrders(): Collection
    {
        $cryptoService = Shop::Container()->getCryptoService();
        $debug         = \defined('PPC_DEBUG') && \PPC_DEBUG;

        return $this->db->getCollection(
            "SELECT tbestellung.kBestellung, tbestellung.dErstellt, tbestellung.cBestellNr,
                       trechnungsadresse.kKunde, trechnungsadresse.cNachname, trechnungsadresse.cVorname,
                       tzahlungsart.kZahlungsart, tzahlungsart.cName AS paymentName,
                       tzahlungsid.txn_id, tzahlungsession.cZahlungsID
                FROM tbestellung
                    INNER JOIN trechnungsadresse ON trechnungsadresse.kRechnungsadresse = tbestellung.kRechnungsadresse
                    INNER JOIN tzahlungsid ON tzahlungsid.kBestellung = tbestellung.kBestellung
                    INNER JOIN tzahlungsart ON tzahlungsart.kZahlungsart = tzahlungsid.kZahlungsart
                    INNER JOIN tpluginzahlungsartklasse ON tpluginzahlungsartklasse.cModulId = tzahlungsart.cModulId
                    LEFT JOIN tzahlungseingang ON tzahlungseingang.kBestellung = tbestellung.kBestellung
                    LEFT JOIN tzahlungsession ON tzahlungsession.kBestellung = tbestellung.kBestellung
                WHERE tpluginzahlungsartklasse.kPlugin = :pluginId
                    AND tzahlungseingang.kBestellung IS NULL
                    AND tbestellung.cAbgeholt = 'P'
                    AND tzahlungsid.txn_id != ''
                    " . ($debug ? '' : 'AND DATE_ADD(tbestellung.dErstellt, INTERVAL 3 HOUR) < NOW()') . '
                ORDER BY tbestellung.kBestellung',
            ['pluginId' => $this->plugin->getID()]
        )->map(static function (stdClass $item) use ($cryptoService) {
            $item->customerName = \trim(
                \trim($cryptoService->decryptXTEA($item->cNachname ?? '')) . ', ' . $item->cVorname,
                ', '
            );

            return $item;
        });
    }

    /**
     * @param int    $paymentId
     * @param string $txnId
     * @return bool
     */
    public function deletePendingPayment(int $paymentId, string $txnId): bool
    {
        $paymentMethod = Helper::getInstance($this->plugin)->getPaymentFromID($paymentId);
        if ($paymentMethod === null) {
            return false;
        }

        $order = $paymentMethod->getPPOrder($txnId);
        $this->db->delete('tzahlungsid', 'txn_id', $txnId);
        if ($order !== null) {
            $paymentMethod->onPaymentComplete($order);
        }

        return true;
    }

    /**
     * @param int                        $paymentId
     * @param string                     $txnId
     * @param AlertServiceInterface|null $as
     * @return bool
     */
    public function applyPendingPayment(int $paymentId, string $txnId, ?AlertServiceInterface $as = null): bool
    {
        $helper        = Helper::getInstance($this->plugin);
        $paymentMethod = $helper->getPaymentFromID($paymentId);
        if ($paymentMethod === null) {
            if ($as !== null) {
                $as->addInfo(
                    \__('Zahlungsart nicht gefunden'),
                    'applyPendingPayment',
                    ['saveInSession' => true]
                );
            }

            return false;
        }
        $order = (new TCCapturePending)->execute($paymentMethod, $paymentMethod->getPPOrder($txnId));
        if ($order === null) {
            if ($as !== null) {
                $as->addInfo(
                    \__('Zahlung %s nicht vorhanden', $txnId),
                    'applyPendingPayment',
                    ['saveInSession' => true]
                );
            }

            return false;
        }
        $shopOrder = $helper->getShopOrder($order);
        if ($shopOrder === null) {
            if ($as !== null) {
                $as->addInfo(
                    \__('Bestellung %s nicht gefunden', $order->getInvoiceId()),
                    'applyPendingPayment',
                    ['saveInSession' => true]
                );
            }

            return false;
        }

        if (\in_array($paymentMethod->getValidOrderState($order), [
            OrderStatus::STATUS_COMPLETED,
            OrderStatus::STATUS_DECLINED
        ], true)) {
            $paymentMethod->handleOrder($order, $shopOrder, true);
        } else {
            if ($as !== null) {
                $as->addInfo(
                    \__('Zahlung %s nicht abgeschlossen', $order->getId()),
                    'applyPendingPayment',
                    ['saveInSession' => true]
                );
            }

            return false;
        }

        $paymentMethod->onPaymentComplete($order);

        return true;
    }
}
